package org.fmavlyutov.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "delete all projects";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

}
