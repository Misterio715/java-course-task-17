package org.fmavlyutov.exception.entity;

public final class EmptyTaskException extends AbstractEntityException {

    public EmptyTaskException() {
        super("Task can not have empty values!");
    }

}
