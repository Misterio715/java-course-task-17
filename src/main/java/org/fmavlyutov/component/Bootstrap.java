package org.fmavlyutov.component;

import org.fmavlyutov.api.repository.ICommandRepository;
import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.*;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.command.project.*;
import org.fmavlyutov.command.system.*;
import org.fmavlyutov.command.task.*;
import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.AbstractException;
import org.fmavlyutov.exception.system.CommandNotFoundException;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.repository.CommandRepository;
import org.fmavlyutov.repository.ProjectRepository;
import org.fmavlyutov.repository.TaskRepository;
import org.fmavlyutov.service.*;
import org.fmavlyutov.util.TerminalUtil;

import static org.fmavlyutov.constant.CommandLineConstant.EXIT;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new SystemAboutCommand());
        registry(new SystemInfoCommand());
        registry(new SystemVersionCommand());
        registry(new SystemHelpCommand());
        registry(new SystemExitCommand());
        registry(new SystemArgumentListCommand());
        registry(new SystemCommandListCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindToProjectCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public void start(String[] args) {
        initData();
        initLogger();
        argumentsProcessing(args);
        commandsProcessing();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        projectService.add(new Project("Some project", "this is project 1", Status.IN_PROGRESS));
        projectService.add(new Project("Another project", "this is project 2", Status.NOT_STARTED));
        projectService.add(new Project("Third project", "this is project 3", Status.COMPLETED));
        taskService.create("First task", "this is task 1");
        taskService.create("Second task", "this is task 2");
        taskService.create("Third task", "this is task 3");
    }

    private void initLogger() {
        loggerService.info("###### PROGRAM IS STARTING  ######");
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                loggerService.info("###### PROGRAM IS SHUTTING DOWN ######")));
    }

    private void argumentsProcessing(String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        for (String arg : args) {
            final AbstractCommand command = commandService.getCommandByArgument(arg);
            if (command == null) {
                System.out.printf("Argument not found. Enter [%s] to see all arguments\n", CommandLineArgument.HELP);
                System.out.println();
            } else {
                command.execute();
            }
        }
    }

    private void commandsProcessing() {
        String arg = "";
        while (!arg.equals(EXIT)) {
            arg = TerminalUtil.nextLine();
            try {
                displayCommand(arg);
                loggerService.command(arg);
                System.out.println("[SUCCESSFUL]\n");
            } catch (AbstractException e) {
                loggerService.error(e);
                System.out.println("[UNSUCCESSFUL: " + e.getLocalizedMessage() + "]\n");
            }
        }
    }

    private void displayCommand(String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        final AbstractCommand command = commandService.getCommandByName(arg);
        if (command == null) {
            throw new CommandNotFoundException(String.format("Command not found. Enter [%s] to see all arguments", CommandLineArgument.HELP));
        }
        command.execute();
    }

}
